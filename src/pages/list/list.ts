import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import faker from 'faker'
import moment from 'moment'

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: any;
  icons: string[];
  items: Array<{name: string, roomNumber: number, avatar: string, checkin: string, exams: Array<{date:string, documentation: string}>}>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.selectedItem = navParams.get('item');

    this.items = []

    for (let index = 0; index < 10; index++) { 
      this.items.push({
        name: faker.name.findName(),
        roomNumber:  Math.floor(Math.random() * 90 + 10),
        avatar: faker.internet.avatar(),
        checkin: moment.duration(Math.floor(Math.random() * 10), "hours").humanize(),
        exams: [
          {
            date: '24/10/2018',
            documentation: faker.lorem.lines(2)
          },
          {
            date: '12/05/2016',
            documentation: faker.lorem.lines(1)
          },
          {
            date: '03/02/2008',
            documentation: faker.lorem.lines(3)
          },
          {
            date: '28/12/2005',
            documentation: faker.lorem.lines(4)
          },
        ]
      })
    }
  }

  itemTapped(event, item) {
    this.navCtrl.push(ListPage, {
      item: item
    });
  }
}
